" Basics {
	syntax on
"}

" Vundle {
    set nocompatible
    filetype off                " required!
    set rtp+=~/.vim/bundle/vundle/
    call vundle#rc()


    Bundle 'gmarik/vundle'
    Bundle 'jistr/vim-nerdtree-tabs'
    Bundle 'scrooloose/nerdtree'
    map <Leader>x <plug>NERDTreeTabsToggle<CR>

    Bundle 'kien/ctrlp.vim'
    let g:ctrlp_working_path_mode = 'ra'
    let g:ctrlp_user_command = 'find %s -type f'

    Bundle 'javacomplete'
    autocmd Filetype java setlocal completefunc=javacomplete#CompleteParamsInfo
    autocmd Filetype java setlocal omnifunc=javacomplete#Complete

    Bundle 'scrooloose/syntastic'
    let g:syntastic_mode_map = { 'mode': 'passive'}
    nmap \s :SyntasticCheck<CR>

    Bundle 'taglist.vim'
    nmap \t :TlistToggle<CR>

    Bundle 'scrooloose/nerdcommenter'

    filetype indent on   " required!
" }

" General {

    nnoremap <Down> <nop>
    nnoremap <Up> <nop>
    nnoremap <Left> <nop>
    nnoremap <Right> <nop>

    nmap \n :setlocal number!<CR>
    nmap \l :setlocal list!<CR>
    nmap \o :set paste!<CR>

    set autoindent
	set ts=4
    set shiftwidth=4
	set expandtab
	set number
	set autochdir	
	set wildmenu	
	set wildmode=list:longest
" }

" VIM UI {
	set hlsearch
    "set cursorline
    nnoremap <Space> :noh<cr>
	set ruler		
	set laststatus=2
	set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][%l,%v]
"}

" Pathogen {
"    call pathogen#infect()
"}

